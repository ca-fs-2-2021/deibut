<?php

$routes = [
  '/' => '\Deividas\Home\Controller\Index',
  'error' => '\Deividas\Error\Controller\Index',
  'products' => '\Deividas\Products\Controller\Index',
  'categories' => '\Deividas\Categories\Controller\Index',
  'contacts' => '\Deividas\Contacts\Controller\Index',
];
