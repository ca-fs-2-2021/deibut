<?php

use Deividas\Framework\Helper\Request;
use Deividas\Framework\Helper\Router;

include_once '../vendor/autoload.php';
include_once '../config/env.php';

$request = new Request();
$url = $request->getRoute();

$router = new Router();
$router->loadController($url);

//1.
// app/code/Deividas/Framework/Helper/Request.php
// Kam getPost() $key setinamas į null?

// 2.
// app/code/Deividas/Framework/Helper/Router.php
// Kodėl klasė $controllerClass aprašoma klasės metodo loadController($url) viduje?
// Iš jos kuriamas objektas?
// Ar teisingai traktuoju, kad po kintamuoju $controllerClass tiesiog slyoi neai6ku kuri $routes masyvo klasė, ir čia iš jos kuriamas objektas dinamiškai?
// Ar neturėtų būti atskira Helperio klasė?