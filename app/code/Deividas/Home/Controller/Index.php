<?php

namespace Deividas\Home\Controller;

use Deividas\Framework\Core\Controller;
use Deividas\Products\Model\Collection\Contacts;

class Index extends Controller
{
    public function __construct()
    {
        parent::__construct('Deividas\Home','form');
    }

    public function index()
    {
        $data['title'] = 'Special offers for today';
        $productsCollection = new Contacts();
        $data['products'] = $productsCollection->getCollection();
        $this->render('form\home', $data);
    }

}