<div class="display-4 text-warning mt-4 mb-2">
    <?= $data['title'] ?>
</div>

<?php foreach ($data['products'] as $product): ?>
    <div class="card shadow my-3 w-50">
        <div class="card-body">
            <h5 class="card-title display-6"><?= $product->getName(); ?></h5>
            <p class="card-text display-4 text-danger">&euro;<?= $product->getSpecialPrice(); ?></p>
            <a class="btn btn-warning" href="#">Order now!</a></td>
        </div>
    </div>
<?php endforeach; ?>




