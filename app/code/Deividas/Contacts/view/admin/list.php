<h1 class="display-6 text-danger mt-4 mb-2"><?= $data['title'] ?></h1>
<table class="table mt-3">
    <thead class="text-danger">
    <th>Name</th>
    <th>Surname</th>
    <th>Email</th>
    <th>Phone</th>
    <th>Subject</th>
    <th>Message</th>
    <th>Topic</th>
    <th>Delete</th>
    </thead>
    <tbody>
    <?php foreach ($data['contacts'] as $contact): ?>
        <tr>
            <td><?= $contact->getName(); ?></td>
            <td><?= $contact->getSurname(); ?></td>
            <td><?= $contact->getEmail(); ?></td>
            <td><?= $contact->getPhone(); ?></td>
            <td><?= $contact->getSubject(); ?></td>
            <td><?= $contact->getMessage(); ?></td>
            <td><?= $contact->getTopicId(); ?></td>
            <td>
                <form method="post" action="/contacts/delete">
                    <input type="hidden" name="id" value="<?= $contact->getId() ?>">
                    <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>


