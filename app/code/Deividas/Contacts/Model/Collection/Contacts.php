<?php

namespace Deividas\Contacts\Model\Collection;

use Deividas\Framework\Helper\SqlBuilder;
use Deividas\Contacts\Model\Contact;

class Contacts
{
    private $collection = [];

    public function getCollection()
    {
        $db = new SqlBuilder();
        $contactIds = $db->select('id')->from('contacts')->get();

        foreach ($contactIds as $row) {
            $contact = new Contact();
            $this->collection[] = $contact->load($row['id']);
        }
        return $this->collection;
    }
}

