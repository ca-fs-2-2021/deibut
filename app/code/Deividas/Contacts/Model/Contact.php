<?php


namespace Deividas\Contacts\Model;


use Deividas\Framework\Helper\SqlBuilder;
use Deividas\Framework\Helper\Validation;

class Contact
{

    private $id;
    private $name;
    private $surname;
    private $email;
    private $phone;
    private $subject;
    private $message;
    private $topic_id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname($surname): void
    {
        $this->surname = $surname;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message): void
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getTopicId()
    {
        return $this->topic_id;
    }

    /**
     * @param mixed $topic_id
     */
    public function setTopicId($topic_id): void
    {
        $this->topic_id = $topic_id;
    }


    private function create()
    {
        $values = [
            'name' => Validation::validString($this->name),
            'surname' => Validation::validString($this->surname),
            'email' => Validation::validEmail($this->email),
            'phone' => Validation::validString($this->phone),
            'subject' => Validation::validString($this->subject),
            'message' => Validation::validString($this->message),
            'topic_id' => Validation::validInteger($this->topic_id),
        ];
        $db = new SqlBuilder();
        $db->insert('contacts')->values($values)->exec();
    }

    public function save()
    {
        if ($this->id) {
            $this->update();
        } else {
            $this->create();
        }
    }

    public function load($id)
    {
        $db = new SqlBuilder();
        $contact = $db->select()->from('contacts')->where('id', Validation::validInteger($id))->getOne();
        $this->id = $contact['id'];
        $this->name = $contact['name'];
        $this->surname = $contact['surname'];
        $this->email = $contact['email'];
        $this->phone = $contact['phone'];
        $this->subject = $contact['subject'];
        $this->message = $contact['message'];
        $this->topic_id = $contact['topic_id'];
        return $this;
    }

    public function delete()
    {
        $db = new SqlBuilder();
        $db->delete()->from('contacts')->where('id', $this->id)->exec();
    }

}
