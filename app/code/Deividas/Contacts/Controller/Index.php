<?php


namespace Deividas\Contacts\Controller;

use Deividas\Contacts\Model\Contact;
use Deividas\Framework\Core\Controller;
use Deividas\Framework\Helper\FormBuilder;
use Deividas\Framework\Helper\Request;
use Deividas\Framework\Helper\Url;
use Deividas\Contacts\Model\Collection\Contacts;

class Index extends Controller
{
    private $post;

    public function __construct()
    {
        $request = new Request();
        $this->post = $request->getPost();
        parent::__construct('Deividas\Contacts', 'form');
    }

    public function index()
    {
        $data['title'] = 'Contact list';
        $contactCollection = new Contacts();
        $data['contacts'] = $contactCollection->getCollection();
        $this->render('admin/list', $data);

    }

    public function create()
    {
        $options = [
            '0' => '-------',
            '1' => 'Products',
            '2' => 'Repair',
            '3' => 'Warranty',
            '4' => 'Other'
        ];
        $data['title'] = 'Leave your request';
        $form = new FormBuilder('POST', Url::getUrl('contacts/store'), 'my-3', '');
        $form
            ->input('text', 'name', 'form-control', 'name', 'Enter your name', '', '', '', 'required')
            ->input('text', 'surname', 'form-control', 'surname', 'Enter your surname', '', '', '', 'required')
            ->input('email', 'email', 'form-control', 'email', 'Enter your email', '', '', '', 'required')
            ->input('text', 'phone', 'form-control', 'phone', 'Enter your phone number')
            ->input('text', 'subject', 'form-control', 'subject', 'Write a subject', '', '', '', 'required')
            ->textarea('message', 'form-control', 'Leave yur message', '', '', 'required')
            ->select('topic_id', 'form-select', $options, '')
            ->button('send', 'btn btn-info mt-3', 'Send');
        $data['form'] = $form->get();
        $this->render('form\create', $data);
    }

    public function store()
    {
        $contact = new Contact();
        $contact->setName($this->post['name']);
        $contact->setSurname($this->post['surname']);
        $contact->setEmail($this->post['email']);
        $contact->setPhone($this->post['phone']);
        $contact->setSubject($this->post['subject']);
        $contact->setMessage($this->post['message']);
        $contact->setTopicId($this->post['topic_id']);
        $contact->save();
        header('Location:' . Url::getUrl('contacts'));
    }

    public function delete()
    {
        $this->id = $this->post['id'];
        $contact = new Contact();
        $contact->load($this->id)->delete();
        header('Location:' . Url::getUrl('contacts'));
    }

}
