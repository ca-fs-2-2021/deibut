<?php

namespace Deividas\Categories\Model\Collection;

use Deividas\Categories\Model\Category;
use Deividas\Framework\Helper\SqlBuilder;

class Categories
{
    private $collection = [];

    public function getCollection()
    {
        $db = new SqlBuilder();
        $categoriesIds = $db->select('id')->from('categories')->get();

        foreach ($categoriesIds as $row) {
            $category = new Category();
            $this->collection[] = $category->load($row['id']);
        }
        return $this->collection;
    }
}
