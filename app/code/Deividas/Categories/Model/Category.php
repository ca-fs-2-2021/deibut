<?php

namespace Deividas\Categories\Model;

use Deividas\Framework\Helper\SqlBuilder;

class Category
{
    private $id;
    private $name;
    private $slug;
    private $parent_id;
    private $products;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setSlug($slug): void
    {
        $this->slug = $slug;
    }

    public function getParentId()
    {
        return $this->parent_id;
    }

    public function setParentId($parent_id): void
    {
        $this->parent_id = $parent_id;
    }

    public function getProducts()
    {
        return $this->products;
    }

    public function setProducts($ids): void
    {
        $this->products = $ids;
    }

    public function load($id)
    {
        $db = new SqlBuilder();
        $category = $db->select()->from('categories')->where('id', $id)->getOne();
        $this->id = $category['id'];
        $this->name = $category['name'];
        $this->slug = $category['slug'];
        $this->parent_id = $category['parent_id'];
        $this->products = $this->getRelationships();
        return $this;
    }

    public function save()
    {
        if (isset($this->id)) {
            $this->update();
        } else {
            $this->create();
        }
    }

    private function create()
    {
        $db = new SqlBuilder();
        $db->insert('categories')->values([
            'name' => $this->name,
            'slug' => $this->slug,
            'parent_id' => $this->parent_id
        ])->exec();
    }

    private function update()
    {
        $values = [
            'name' => $this->name,
            'slug' => $this->slug,
            'parent_id' => $this->parentId
        ];

        $this->clearRelationsToProducts();
        $this->assignProducts();
        $db = new SqlBuilder();
        $db->update('categories')->set($values)->where('id', $this->id)->exec();
    }

    public function delete()
    {
        $db = new SqlBuilder();
        $db->delete()->from('categories')->where('id', $this->id)->exec() ;
    }

    private function assignProducts()
    {
        foreach ($this->products as $productId) {
            $db = new SqlBuilder();

            $db->insert('products_categories')->values([
                'product_id' => $productId,
                'category_id' => $this->id
            ])->exec();
        }
    }

    public function clearRelationsToProducts()
    {
        $db = new SqlBuilder();
        $db->delete()->from('products_categories')->where('category_id', $this->id)->exec();
    }

    private function getRelationships()
    {
        $productIds = [];
        $db = new SqlBuilder();
        $products = $db->select()->from('products_categories')->where('category_id', $this->id)->get();
        foreach ($products as $id) {
            $productIds[] = $id['product_id'];
        }
        return $productIds;
    }

}