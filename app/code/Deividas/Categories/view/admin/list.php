<h1 class="display-6 text-danger mt-4 mb-2"><?= $data['title'] ?></h1>
<table class="table mt-3">
    <thead class="text-danger">
        <th>ID</th>
        <th>Name</th>
        <th>Slug</th>
        <th>Parent ID</th>
        <th colspan="2">Actions</th>
    </thead>
    <tbody>
    <?php foreach ($data['categories'] as $category): ?>
        <tr>
            <td><?= $category->getId(); ?></td>
            <td><?= $category->getName(); ?></td>
            <td><?= $category->getSlug(); ?></td>
            <td><?= $category->getParentId(); ?></td>
            <td><a class="btn btn-info" href="/categories/edit/<?= $category->getId() ?>">Edit</a></td>
            <td>
                <form method="post" action="/categories/delete">
                    <input type="hidden" name="id" value="<?= $category->getId() ?>">
                    <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>