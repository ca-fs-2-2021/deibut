<div class="vh-100">
    <h1 class="display-6 text-success mt-4 mb-2"><?= $data['title'] ?></h1>
    <?= $data['form'] ?>
</div>