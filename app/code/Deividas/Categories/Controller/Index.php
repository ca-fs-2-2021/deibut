<?php

namespace Deividas\Categories\Controller;

use Deividas\Categories\Model\Collection\Categories;
use Deividas\Framework\Core\Controller;
use Deividas\Framework\Helper\FormBuilder;
use Deividas\Framework\Helper\Request;
use Deividas\Categories\Model\Category;
use Deividas\Framework\Helper\Url;
use Deividas\Products\Model\Collection\Contacts;


class Index extends Controller
{
    private $post;

    public function __construct()
    {
        $request = new Request();
        $this->post = $request->getPost();
        parent::__construct('Deividas\Categories', 'form');
    }

    public function index()
    {
        $data['title'] = 'Category list';
        $categoriesCollection = new Categories();
        $data['categories'] = $categoriesCollection->getCollection();
        $this->render('admin/list', $data);
    }

    public function edit($id)
    {
        $data['title'] = 'Update category';

        $category = new Category();
        $category->load($id);

        $form = new FormBuilder('post', '/categories/store', 'mb-2', '');
        $products = new Contacts();
        $form
            ->input('hidden', 'id', '', '', '', '', $category->getId())
            ->input('text', 'name', 'form-control', 'name', 'Enter category name', 'Category name', $category->getName())
            ->input('text', 'slug', 'form-control', 'slug', 'Enter category slug', 'Category slug', $category->getSlug())
            ->input('text', 'parent_id', 'form-control', 'parent_id', 'Enter category parent ID', 'Category parent ID', $category->getParentId());

        foreach ($products->getCollection() as $product) {
            in_array($product->getId(), $category->getProducts()) ? $checked = true : $checked = false;
            $form->input(
                'checkbox',
                'products[]',
                'form-check-input mx-2',
                'product' . $product->getId(),
                '',
                $product->getName(),
                $product->getId(),
                $checked
            );
        }
        $form->button('update', 'btn btn-info mt-3', 'Update');

        $data['form'] = $form->get();

        $deleteForm = new FormBuilder('post', '/categories/delete');
        $deleteForm
            ->input('hidden', 'id', '', 'id', '', '', $category->getId())
            ->button('delete', 'btn btn-danger', 'Delete');
        $data['form2'] = $deleteForm->get();
        $this->render('form\edit', $data);
    }

    public function create()
    {
        $products = new Contacts();

        $data['title'] = 'Create category';

        $categories = new Categories();
        $categoriesOptions = [];
        $categoriesOptions[0] = 'Select';
        foreach ($categories->getCollection() as $category) {
            $categoriesOptions[$category->getId()] = $category->getName();
        }
        $form = new FormBuilder('post', '/categories/store', 'my-3');
        $form
            ->input('text', 'name', 'form-control', 'name', 'Enter category name')
            ->input('text', 'slug', 'form-control', 'slug', 'Enter category slug')
            ->select('parent_id', 'form-select', $categoriesOptions);

        foreach ($products->getCollection() as $product) {
            in_array($product->getId(), $category->getProducts()) ? $checked = true : $checked = false;
            $form->input(
                'checkbox',
                'products[]',
                'form-check-input mx-2',
                'product' . $product->getId(),
                '',
                $product->getName(),
                $product->getId(),
                $checked
            );
        }

        $form->button('save', 'btn btn-success my-3', 'Save');

        $data['form'] = $form->get();
        $this->render('form/create', $data);
    }

    public function store()
    {
        $category = new Category();
        $this->id = $this->post['id'];
        if (isset($this->id)) {
            $category->load($this->id);
        }
        $category->setName($this->post['name']);
        $category->setSlug($this->post['slug']);
        $category->setParentId($this->post['parent_id']);
        $category->save();
        header('Location:' . Url::getUrl('categories'));
    }

    public function delete()
    {
        $this->id = $this->post['id'];
        $category = new Category();
        $category->load($this->id)->delete();
        header('Location:' . Url::getUrl('categories'));
    }

}