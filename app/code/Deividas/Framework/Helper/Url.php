<?php

namespace Deividas\Framework\Helper;

class Url
{
    public static function getUrl($path)
    {
        return APP_DOMAIN . $path;
    }

    public static function getCssUrl($file)
    {
        return APP_DOMAIN . 'css/' . $file . '.css';
    }

    public static function getJsUrl($file)
    {

    }

    public static function getImageUrl($file)
    {

    }



}