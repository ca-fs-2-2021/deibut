<?php

namespace Deividas\Framework\Helper;

class FormBuilder
{
    private $form = '';


    public function __construct($method, $action, $class = '', $id = '')
    {
        $this->form .= "<form method='$method' action='$action' class='$class' id='$id'>";
        return $this;
    }

    public function input($type, $name, $class = '', $id = '', $placeholder = '', $label = '', $value = '', $checked = false, $required = '')
    {
        if ($label !== '' && $id !== '') {
            $this->form .= "<label for='$id'>$label</label>";
        }
        $checked ? $check = 'checked' : $check = '';
        $this->form .= "<input type='$type' name='$name' class='$class' id='$id' placeholder='$placeholder' value='$value' $check required = '$required'><br>";

        return $this;
    }

    public function select($name, $class, $options, $label = '')
    {
        if ($label !== '') {
            $this->form .= "<label for ='$name'>$label</label>";
        }
        $this->form .= "<select name='$name' $class>";
        foreach ($options as $id => $option) {
            $this->form .= "<option value='$id'>$option</option>";
        }
        $this->form .= "</select><br>";
        return $this;
    }

    public function textarea($name, $class = '', $placeholder, $label, $value = '', $required = '')
    {
        $this->form .= "<textarea name='$name' class='$class' placeholder='$placeholder' required = '$required' label='$label'>$value</textarea><br>";
        return $this;
    }


    public function button($name, $class = '', $text)
    {
        $this->form .= "<button name='$name' class='$class'>$text</button><br>";
        return $this;
    }

    public function get()
    {
        return $this->form . '</form>';
    }
}
