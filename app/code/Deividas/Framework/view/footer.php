</main>
<footer class="bg-secondary text-center text-white">
    <div class="text-center p-3">
        © <?= date("Y"); ?>
        <a class="text-white" href="https://grocery-kiosk.lt/">Grocery Kiosk</a>
    </div>
</footer>

</body>

</html>