<h1 class="display-6 text-danger mt-4 mb-2"><?= $data['title'] ?></h1>
<table class="table mt-3">
    <thead class="text-danger">
        <th>Name</th>
        <th>Descriprion</th>
        <th>SKU</th>
        <th>Price</th>
        <th>Special price</th>
        <th>Cost</th>
        <th>Quantity</th>
        <th colspan="2">Actions</th>
    </thead>
    <tbody>
    <?php foreach ($data['products'] as $product): ?>
    <tr>
        <td><?= $product->getName(); ?></td>
        <td><?= $product->getDescription(); ?></td>
        <td><?= $product->getSku(); ?></td>
        <td><?= $product->getPrice(); ?></td>
        <td><?= $product->getSpecialPrice(); ?></td>
        <td><?= $product->getCost(); ?></td>
        <td> <?= $product->getQty(); ?></td>
        <td><a class="btn btn-info" href="/products/edit/<?= $product->getId() ?>">Edit</a></td>
        <td>
            <form method="post" action="/products/delete">
                <input type="hidden" name="id" value="<?= $product->getId() ?>">
                <button class="btn btn-danger" type="submit">Delete</button>
            </form>
        </td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>

