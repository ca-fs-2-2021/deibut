<?php

namespace Deividas\Products\Model\Collection;

use Deividas\Framework\Helper\SqlBuilder;
use Deividas\Products\Model\Product;

class Products
{
    private $collection = [];

    public function getCollection()
    {
        $db = new SqlBuilder();
        $productsIds = $db->select('id')->from('products')->get();

        foreach ($productsIds as $row) {
            $product = new Product();
            $this->collection[] = $product->load($row['id']);
        }
        return $this->collection;
    }
}
