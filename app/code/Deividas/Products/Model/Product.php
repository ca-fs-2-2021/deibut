<?php

namespace Deividas\Products\Model;

use \Deividas\Framework\Helper\SqlBuilder;
use Deividas\Framework\Helper\Validation;

class Product
{
    private $id;
    private $name;
    private $description;
    private $sku;
    private $price;
    private $specialPrice;
    private $cost;
    private $qty;
    private $skipCategories = false;
    private $categories;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description): void
    {
        $this->description = $description;
    }

    public function getSku()
    {
        return $this->sku;
    }

    public function setSku($sku): void
    {
        $this->sku = $sku;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price): void
    {
        $this->price = $price;
    }

    public function getSpecialPrice()
    {
        return $this->specialPrice;
    }

    public function setSpecialPrice($specialPrice): void
    {
        $this->specialPrice = $specialPrice;
    }

    public function getCost()
    {
        return $this->cost;
    }

    public function setCost($cost): void
    {
        $this->cost = $cost;
    }

    public function getQty()
    {
        return $this->qty;
    }

    public function setQty($qty): void
    {
        $this->qty = $qty;
    }

    public function setSkipCategories($value)
    {
        $this->skipCategories = $value;
    }

    public function getCategories()
    {
        return $this->categories;
    }

    public function setCategories($ids): void
    {
        $this->categories = $ids;
    }

    public function load($id)
    {
        $db = new SqlBuilder();
        $product = $db->select()->from('products')->where('id', Validation::validInteger($id))->getOne();
        $this->id = $product['id'];
        $this->name = $product['name'];
        $this->description = $product['description'];
        $this->sku = $product['sku'];
        $this->price = $product['price'];
        $this->specialPrice = $product['special_price'];
        $this->cost = $product['cost'];
        $this->qty = $product['qty'];
        $this->categories = $this->getRelationships();
        return $this;
    }

    public function loadBySku($sku)
    {
        $db = new SqlBuilder();
        $product = $db->select()->from('products')->where('sku', Validation::validString($sku))->getOne();
        $this->id = $product['id'];
        $this->name = $product['name'];
        $this->description = $product['description'];
        $this->sku = $product['sku'];
        $this->price = $product['price'];
        $this->specialPrice = $product['special_price'];
        $this->cost = $product['cost'];
        $this->qty = $product['qty'];
        return $this;
    }

    public function save()
    {
        if ($this->id) {
            $this->update();
        } else {
            $this->create();
        }
    }

    private function create()
    {
        $values = [
            'name' => Validation::validString($this->name),
            'description' => Validation::validString($this->description),
            'sku' => Validation::validString($this->sku),
            'price' => $this->price,
            'special_price' => $this->specialPrice,
            'cost' => $this->cost,
            'qty' => Validation::validString($this->qty)
        ];
        $db = new SqlBuilder();
        $db->insert('products')->values($values)->exec();
    }

    private function update()
    {
        $values = [
            'name' => Validation::validString($this->name),
            'description' => Validation::validString($this->description),
            'sku' => Validation::validString($this->sku),
            'price' => $this->price,
            'special_price' => $this->specialPrice,
            'cost' => $this->cost,
            'qty' => Validation::validString($this->qty)
        ];
        if ($this->skipCategories){
            $this->clearRelationsToCategories();
            $this->assignCategories();
        }
        $db = new SqlBuilder();
        $db->update('products')->set($values)->where('id', $this->id)->exec();
    }

    public function delete()
    {
        $db = new SqlBuilder();
        $db->delete()->from('products')->where('id', $this->id)->exec() ;
    }

    private function assignCategories()
    {
        foreach ($this->categories as $categoryId) {
            $db = new SqlBuilder();

            $db->insert('products_categories')->values([
                'category_id' => $categoryId,
                'product_id' => $this->id
            ])->exec();
        }
    }

    public function clearRelationsToCategories()
    {
        $db = new SqlBuilder();
        $db->delete()->from('products_categories')->where('product_id', $this->id)->exec();
    }

    private function getRelationships()
    {
        $categoryIds = [];
        $db = new SqlBuilder();
        $categories = $db->select()->from('products_categories')->where('product_id', $this->id)->get();
        foreach ($categories as $id) {
            $categoryIds[] = $id['category_id'];
        }
        return $categoryIds;
    }

    public static function isSkuUnique($sku)
    {
        $db = new SqlBuilder();
        $result = $db->select()->from('products')->where('sku', $sku)->get();
        return empty($result) ? true : false;
    }

}