<?php

namespace Deividas\Products\Controller;

use Deividas\Framework\Core\Controller;
use Deividas\Framework\Helper\FormBuilder;
use Deividas\Framework\Helper\Request;
use Deividas\Framework\Helper\Url;
use Deividas\Products\Model\Collection\Contacts;
use Deividas\Products\Model\Product;
use Deividas\Categories\Model\Collection\Categories;

class Index extends Controller
{
    private $post;
    public const PRODUCT_IMPORT_XML_URL = 'https://manopora.lt/ca/spc.xml';
    public const PRODUCT_IMPORT_STOCKS_CSV_URL = 'https://manopora.lt/ca/stocks.csv';

    public function __construct()
    {
        $request = new Request();
        $this->post = $request->getPost();
        parent::__construct('Deividas\Products', 'form');
    }

    public function index()
    {
        $data['title'] = 'Product list';
        $productsCollection = new Contacts();
        $data['products'] = $productsCollection->getCollection();
        $this->render('admin/list', $data);
    }

    public function edit($id)
    {
        $data['title'] = 'Update product';

        $product = new Product();
        $product->load($id);

        $form = new FormBuilder('post', '/products/store', 'mb-2', '');
        $categories = new Categories();
        $form
            ->input('hidden', 'id', '', 'id', '', '', $product->getId())
            ->input('text', 'name', 'form-control', 'name', 'Enter product name', 'Product name', $product->getName())
            ->textarea('description', 'form-control', 'Enter product description', 'Product description', $product->getDescription())
            ->input('text', 'sku', 'form-control', 'sku', 'Enter product SKU', 'Product SKU', $product->getSku())
            ->input('number', 'price', 'form-control', 'price', 'Enter product price', 'Product price', $product->getPrice())
            ->input('number', 'special_price', 'form-control', 'special_price', 'Enter product special price', 'Product special price', $product->getSpecialPrice())
            ->input('number', 'cost', 'form-control', 'cost', 'Enter product cost', 'Product cost', $product->getCost())
            ->input('number', 'qty', 'form-control', 'qty', 'Enter your local warehouse product quantity', 'Quantity in your local warehouse', $product->getQty());

        foreach ($categories->getCollection() as $category) {
            in_array($category->getId(), $product->getCategories()) ? $checked = true : $checked = false;
            $form->input(
                'checkbox',
                'categories[]',
                'form-check-input mx-2',
                'category' . $category->getId(),
                '',
                $category->getName(),
                $category->getId(),
                $checked
            );
        }
        $form->button('update', 'btn btn-info mt-3', 'Update');

        $data['form'] = $form->get();

        $deleteForm = new FormBuilder('post', Url::getUrl('products/delete'));
        $deleteForm
            ->input('hidden', 'id', '', 'id', '', '', $product->getId())
            ->button('delete', 'btn btn-danger', 'Delete');
        $data['form2'] = $deleteForm->get();
        $this->render('form\edit', $data);
    }

    public function create()
    {
        $categories = new Categories();

        $data['title'] = 'Create product';
        $form = new FormBuilder('POST', Url::getUrl('products/store'), 'my-3', '');
        $form
            ->input('text', 'name', 'form-control', 'name', 'Enter product name')
            ->textarea('description', 'form-control', 'Enter product description', '')
            ->input('text', 'sku', 'form-control', 'sku', 'Enter product SKU', '')
            ->input('number', 'price', 'form-control', 'price', 'Enter product price', '')
            ->input('number', 'special_price', 'form-control', 'special_price', 'Enter product special price', '')
            ->input('number', 'cost', 'form-control', 'cost', 'Enter product cost', '')
            ->input('number', 'qty', 'form-control', 'qty', 'Enter product quantity', '');

        foreach ($categories->getCollection() as $category) {
            $form->input(
                'checkbox',
                'categories[]',
                'form-check-input mx-2',
                'category' . $category->getId(),
                '',
                $category->getName(),
                $category->getId()
            );
        }

        $form->button('save', 'btn btn-success mt-3', 'Save');

        $data['form'] = $form->get();
        $this->render('form\create', $data);
    }

    public function store()
    {
        $product = new Product();
        if (isset($this->post['id'])) {
            $product = $product->load($this->post['id']);
        }
        if ($product->getSku() === $this->post['sku'] || Product::isSkuUnique((string)$this->post['sku'])) {
            $product->setName($this->post['name']);
            $product->setDescription($this->post['description']);
            $product->setSku($this->post['sku']);
            $product->setPrice($this->post['price']);
            $product->setQty($this->post['qty']);
            $product->setCost($this->post['cost']);
            $product->setCategories($this->post['categories']);
            $product->save();
            header('Location:' . Url::getUrl('products'));
        } else {
            echo 'SKU '. $this->post['sku'] . ' jau yra užimtas';
        }
    }

    public function delete()
    {
        $this->id = $this->post['id'];
        $product = new Product();
        $product->load($this->id)->delete();
        header('Location:' . Url::getUrl('products'));
    }

    public function import()
    {
        $pathToSave = PROJECT_ROOT_DIR . 'var\import\\';
        $fileName = 'products' . date('d-m-Y') . '.xml';
        $file = $pathToSave . $fileName;
        $fp = fopen($file, 'w+');
        $curlHandler = curl_init(self::PRODUCT_IMPORT_XML_URL);
        curl_setopt($curlHandler, CURLOPT_FILE, $fp);
        curl_exec($curlHandler);

        $xml = simplexml_load_file($file, 'SimpleXMLElement', LIBXML_NOCDATA);

        foreach ($xml->produkt as $product) {
            $productObject = new Product();
            $name = (string)$product->nazwa;
            $description = (string)$product->dlugi_opis;
            $sku = (string)$product->kzs;
            if (!Product::isSkuUnique($sku)){
                continue;
            }
            header('Location:' . Url::getUrl('products'));
            $price = self::convertPlnToEur((float)$product->cena_zewnetrzna);
            $cost = self::convertPlnToEur((float)$product->cena_zewnetrzna_hurt);
            $qty = (int)$product->status;
            if ($qty == 0) {
                continue;
            }
            $productObject->setName($name);
            $productObject->setDescription($description);
            $productObject->setSku($sku);
            $productObject->setPrice($price);
            $productObject->setCost($cost);
            $productObject->setQty($qty);

            $productObject->save();

            header('Location:' . Url::getUrl('products'));
        }
    }

    private static function convertPlnToEur($price)
    {
        return $price * 0.22;
    }

    public function stocks()
    {
        $pathToSave = PROJECT_ROOT_DIR . 'var\import\stocks\\';
        $fileName = 'products_stocks' . date('d-m-Y') . '.csv';
        $file = $pathToSave . $fileName;
        $fp = fopen($file, 'w+');
        $curlHandler = curl_init(self::PRODUCT_IMPORT_STOCKS_CSV_URL);
        curl_setopt($curlHandler, CURLOPT_FILE, $fp);
        curl_exec($curlHandler);

        $stocks = array_map('str_getcsv', file($file));
        array_walk($stocks, function (&$a) use ($stocks) {
            $a = array_combine($stocks[0], $a);
        });
        array_shift($stocks);

        foreach ($stocks as $stockItem) {
            if (!Product::isSkuUnique($stockItem['sku'])) {
                $product = new Product();
                $product->loadBySku($stockItem['sku']);
                $product->setQty($stockItem['qty']);
                $product->setSkipCategories(true);
                $product->save();
            }
        }
        header('Location:' . Url::getUrl('products'));
    }

    public function export()
    {
        $productCollection = new Contacts();
        $productCollection = $productCollection->getCollection();
        $productsToCsv = [];
        $productsToCsv[] = ['id','name','sku','price', 'qty'];
        foreach ($productCollection as $product){
            $productsToCsv[] = [
                $product->getId(),
                $product->getName(),
                $product->getSku(),
                $product->getPrice(),
                $product->getQty(),
            ];

        }
        $pathToSave = PROJECT_ROOT_DIR . 'var\export\\';
        $fileName = 'products' . date('d-m-Y') . '.csv';
        $file = $pathToSave . $fileName;

        $fp = fopen($file, 'w');

        foreach ($productsToCsv as $line) {
            fputcsv($fp, $line);
        }
        fclose($fp);

//        mail('deividas.butkus@hotmail.com', 'Export completed', 'Va tau ir meilas');
    }

}
